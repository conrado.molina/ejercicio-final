# Ejercicio Final

En el siguiente trabajo se encuentra el ejercicio final de  la capacitacion de NAS.
Lo primero que realice fue armar el playbook  donde  en el mismo le pasaba un archivo .j2 que contenia una tarea con textfsm() para  obtener las metricas de la PC  y filtrar valores de transmision y recepcion. Para buscar las metricas dentro de la PC use el comando ifconfig.
Una vez que obtuve las metricas, con el playbook las pase a un  archivo .json, que luego lo utilice para llenar la base de datos de influxDB. A la base de datos de influx DB la llene con el comando  `insert`.
Y por ultimo una vez que termine de armar la base de datos, me pase a armar  el DAG para poder correr el playbook, y observe en `localhost:8080` que aparezca la DAG y se realice la tarea. Una vez que corrio todo, di por finalizado el ejercicio.
