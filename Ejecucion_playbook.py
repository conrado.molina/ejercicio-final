from airflow.utils.dates import days_ago
from airflow import DAG
from airflow.operators.bash import BashOperator
with DAG(
    'Ejercicio_final',
    description='Abrir Playbook',
    start_date=days_ago(0),
    tags=['prueba']
) as dag:

    t1= BashOperator(
        task_id='Ejecutar',
        bash_command='ansible-playbook /home/conrado/Ejercicio_final/playbook.yml'
    )
t1
